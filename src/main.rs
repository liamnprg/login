#[macro_use] extern crate yew;


//use yew::html::*;
use yew::prelude::*;

struct Model {
    users: Vec<(String,String)>,
}

enum Msg {
    Login(String,String)
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();
    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        Model { 
            users: vec!((String::from("jeff"),String::from("kaplan")))
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Login(u1,u2) => {
                self.users = vec!((u1,u2));
                true
            }
        }
    }
}

impl Renderable<Model> for Model {
    fn view(&self) -> Html<Self> {
        html! {
            // Render your model here
            <p>{for self.users.iter().map(Renderable::view)}</p>
            <button onclick=|_| Msg::Login("Hello".to_string(),"World".to_string()),>{ "Click me!" }</button>
        }
    }
}

impl Renderable<Model> for (String,String) {
    fn view(&self) -> Html<Model> {
        html! {
            <p>{ format!("Tiblet: {},{}", self.0, self.1) }</p>
        }
    }

}

fn main() {
    yew::initialize();
    App::<Model>::new().mount_to_body();
    yew::run_loop();
}
